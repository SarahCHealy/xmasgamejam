﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ActivityMang : MonoBehaviour 
{
	public GameObject exclamation1;
	public GameObject exclamation2;
	public GameObject exclamation3;

	public GameObject convo1;
	CharacterScript convo1Script;
	public GameObject convo2;
	CharacterScript convo2Script;
	public GameObject convo3;
	CharacterScript convo3Script;

	public GameObject baby;
	BabyScript cryScript;

	bool startConversations;

	bool ex1Showing;
	bool ex2Showing;
	bool ex3Showing;
	bool babyCrying;

	float merryMeter;

	public GameObject meter;
	// Use this for initialization
	// Use this for initialization
	IEnumerator Start () 
	{
		convo1Script = convo1.GetComponent<CharacterScript> ();
		convo2Script = convo2.GetComponent<CharacterScript> ();
		convo3Script = convo3.GetComponent<CharacterScript> ();

		cryScript = baby.GetComponent<BabyScript> ();

		exclamation1.SetActive (true);
		yield return new WaitForSeconds(10f);
		exclamation2.SetActive (true);
		yield return new WaitForSeconds(5f);
		exclamation3.SetActive (true);
		startConversations=true;
		StartCoroutine (CheckExclamations());
		yield return new WaitForSeconds(30f);
		cryScript.babyCrying = true;

	}

	// Update is called once per frame
	void Update ()
	{
		if (merryMeter < 0f) 
		{
			meter.GetComponent<Slider> ().value = 0f;
		}
		else if (merryMeter >= 1f)
		{
			SceneManager.LoadScene ("New Scene");
			Debug.Log ("end");
		}
		else 
		{
			meter.GetComponent<Slider> ().value = merryMeter;
		}


		if (startConversations) 
		{
			if (!convo1Script.finishedResponding && !convo1Script.responding) 
			{
				exclamation1.SetActive (true);
				ex1Showing = true;
			}
			else 
			{
				exclamation1.SetActive (false);
				ex1Showing = false;
			}

			if (!convo2Script.finishedResponding && !convo2Script.responding)
			{
				exclamation2.SetActive (true);
				ex2Showing = true;
			}
			else 
			{
				exclamation2.SetActive (false);
				ex2Showing = false;
			}

			if (!convo3Script.finishedResponding && !convo3Script.responding)
			{
				exclamation3.SetActive (true);
				ex3Showing = true;
			}
			else 
			{
				exclamation3.SetActive (false);
				ex3Showing = false;
			}
		}
	}

	IEnumerator CheckExclamations()
	{
		while (true) 
		{
			if (startConversations) 
			{
				if (ex1Showing) 
				{
					merryMeter = merryMeter + 0.03f;
				}
				else
				{
					merryMeter = merryMeter - 0.03f;
				}
				if (ex2Showing) 
				{
					merryMeter = merryMeter + 0.03f;
				}
				else
				{
					merryMeter = merryMeter - 0.03f;
				}
				if (ex3Showing) 
				{
					merryMeter = merryMeter + 0.03f;
				}

				{
					merryMeter = merryMeter - 0.03f;
				}
				if (cryScript.babyCrying) 
				{
					merryMeter = merryMeter + 0.05f;
				}
				yield return new WaitForSeconds(1f);
			}


		}
	}
}
