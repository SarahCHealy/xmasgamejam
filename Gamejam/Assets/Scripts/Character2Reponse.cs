﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Character2Reponse : MonoBehaviour 
{
	public GameObject convoPartner;
	CharacterScript char1Script;

	public string [] fights;

	public string[] responses;//0 sided w/other player, 1 sided with them, 2 netruel

	public bool receivedNegResponse;
	public bool finishedResponding;

	public GameObject speechBox;
	public Text speechText;
	TutorialText speechScript;

	int angerMeter;

	public GameObject options;

	// Use this for initialization
	void Start () 
	{
		char1Script = convoPartner.GetComponent<CharacterScript> ();
		speechScript = speechText.GetComponent<TutorialText> ();
	}
	
	// Update is called once per frame
	void Update () {}

	public IEnumerator RespondToFight()
	{
		speechScript.message = fights [char1Script.convoCycle];
		speechBox.SetActive (true);
		StartCoroutine (speechScript.TypeText ());
		yield return new WaitForSeconds(15f);
		speechBox.SetActive(false);
		options.SetActive (true);
	}

	public IEnumerator RespondToPlayer()
	{
		if (!receivedNegResponse && char1Script.receivedNegResponse)//sided with char1
		{
			speechBox.SetActive (true);
			speechScript.message =  responses [0];
			StartCoroutine (speechScript.TypeText ());
			Debug.Log ("char2: " + speechScript.message);
			yield return new WaitForSeconds(15f);
			speechBox.SetActive(false);
		}
		if (receivedNegResponse && !char1Script.receivedNegResponse)//sided with chr1
		{
			speechBox.SetActive (true);
			speechScript.message =  responses [1];
			StartCoroutine (speechScript.TypeText ());
			Debug.Log ("char2: " + speechScript.message);
			yield return new WaitForSeconds(15f);
			speechBox.SetActive(false);
		}
		if (!receivedNegResponse && !char1Script.receivedNegResponse)//netruel
		{
			
			speechBox.SetActive (true);
			speechScript.message =  responses [2];
			StartCoroutine (speechScript.TypeText ());
			Debug.Log ("char2: " + speechScript.message);
			yield return new WaitForSeconds(15f);
			speechBox.SetActive(false);
		}
	}
}
