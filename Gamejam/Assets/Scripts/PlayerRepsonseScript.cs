﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerRepsonseScript : MonoBehaviour 
{
	public GameObject char1;
	CharacterScript char1Script;

	public GameObject char2;
	Character2Reponse char2Script;

	public string[] responses;//0 sided char 1, 1 sided with char 2, 2 netruel

	public GameObject options;

	// Use this for initialization
	void Start () 
	{
		char1Script = char1.GetComponent<CharacterScript> ();
		char2Script = char2.GetComponent<Character2Reponse> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
		
	public void PlayerRespond (string option)
	{
		options.SetActive (false);
		if (option == "a")//side against char1
		{
			char1Script.receivedNegResponse = true;
			char2Script.receivedNegResponse = false;
		}
		else if (option == "b") //side against char2
		{
			char1Script.receivedNegResponse = false;
			char2Script.receivedNegResponse = true;
		}
		else if  (option == "c")//neutrel
		{
			char1Script.receivedNegResponse = false;
			char2Script.receivedNegResponse = false;
		}
		StartCoroutine (char1Script.RespondToPlayer());
	}
}
