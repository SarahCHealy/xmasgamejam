﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour 
{
	Scene thisScene;
	AudioSource audioSrc;
	// Use this for initialization
	void Start () 
	{
		thisScene = SceneManager.GetActiveScene();
		audioSrc=GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (thisScene.name == "Introduction") 
		{
			if (!audioSrc.isPlaying)
			{
				SceneManager.LoadScene ("Scene1");
			}
		}

		if (thisScene.name == "New Scene") 
		{
			if (Input.GetMouseButton (0)) 
			{
				SceneManager.LoadScene ("StartScene");
			}
		}

	}

	public void GoToIntro()
	{
		SceneManager.LoadScene ("Introduction");
	}
}
