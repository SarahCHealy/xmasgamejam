﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BabyScript : MonoBehaviour 
{
	public bool babyCrying;
	bool mouseInTrigger;
	int cryCount;

	AudioSource audioSrc;

	public GameObject slider;
	// Use this for initialization
	void Start () 
	{
		audioSrc=GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		if (mouseInTrigger && Input.GetMouseButton (0) && babyCrying) 
		{
			cryCount++;
		}
		else if (babyCrying && cryCount > 0) 
		{
			cryCount--;
		}

		if (cryCount == 100)
		{

			slider.SetActive (false);
			babyCrying = false;
			cryCount = 0;
			audioSrc.Stop ();
			StartCoroutine (RestartCountDownForBabyCry ());
		}

		if (babyCrying) 
		{
			if (!audioSrc.isPlaying) 
			{
				audioSrc.Play ();
			}
			slider.SetActive (true);
			slider.GetComponent<Slider> ().value = cryCount;
		}
	}

	void OnMouseEnter()
	{
		mouseInTrigger = true;
	}
	void OnMouseExit()
	{
		mouseInTrigger = false;
	}

	IEnumerator RestartCountDownForBabyCry()
	{
		yield return new WaitForSeconds(30f);
		babyCrying = true;

	}
}
