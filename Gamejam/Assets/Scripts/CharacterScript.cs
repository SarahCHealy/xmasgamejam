﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterScript : MonoBehaviour 
{
	int angerMeter;
	public bool fightStarted;

	public GameObject convoPartner;
	Character2Reponse char2Script;

	public string [] fights;
	public int convoCycle;

	public string[] responses;//0 side with cahr1, side with char 2, 2 netruel

	public bool receivedNegResponse;
	public bool finishedResponding;
	public bool responding;

	public GameObject speechBox;
	public Text speechText;
	TutorialText speechScript;

	public float waitTimeBetweenChars=5f;

	// Use this for initialization
	void Start () 
	{
		char2Script = convoPartner.GetComponent<Character2Reponse> ();
		speechScript = speechText.GetComponent<TutorialText> ();
	}
	
	// Update is called once per frame
	void Update () {}


	public IEnumerator StartFight()
	{
		responding = true;
		speechScript.message = fights [convoCycle];
		speechBox.SetActive (true);
		StartCoroutine (speechScript.TypeText ());
		yield return new WaitForSeconds(waitTimeBetweenChars);
		speechBox.SetActive(false);
		StartCoroutine (char2Script.RespondToFight());
	}

	public IEnumerator RespondToPlayer()
	{
		if (!receivedNegResponse && char2Script.receivedNegResponse)//sided with char1
		{
			StartCoroutine (char2Script.RespondToPlayer());
			yield return new WaitForSeconds(waitTimeBetweenChars+2f);
			speechBox.SetActive (true);
			speechScript.message =  responses [0];
			StartCoroutine (speechScript.TypeText ());
			Debug.Log ("char1: " + speechScript.message);
			yield return new WaitForSeconds(waitTimeBetweenChars);
			speechBox.SetActive(false);
		}
		if (receivedNegResponse && !char2Script.receivedNegResponse)//sided with cahr1
		{
			speechBox.SetActive (true);
			speechScript.message =  responses [1];
			StartCoroutine (speechScript.TypeText ());
			Debug.Log ("char1: " + speechScript.message);
			yield return new WaitForSeconds(waitTimeBetweenChars);
			speechBox.SetActive(false);
			StartCoroutine (char2Script.RespondToPlayer());
		}
		if (!receivedNegResponse && !char2Script.receivedNegResponse)//netruel
		{
			speechBox.SetActive (true);
			speechScript.message =  responses [2];
			StartCoroutine (speechScript.TypeText ());
			Debug.Log ("char1: " + speechScript.message);
			yield return new WaitForSeconds(waitTimeBetweenChars);
			speechBox.SetActive(false);
			StartCoroutine (char2Script.RespondToPlayer());
		}

		if (convoCycle >= fights.Length) 
		{
			convoCycle++;
		}
		else 
		{
			convoCycle = 0;
		}

		responding = false;
		finishedResponding = true;
		yield return new WaitForSeconds(15f);
		finishedResponding = false;
	}

	public void StartConversation ()
	{
		StartCoroutine (StartFight ());
	}
}
